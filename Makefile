.PHONY = test test-jit clean clean-zeek
INPUT = test-messages/datetime_hex/datetime_hello.hex test-messages/datetime_hex/datetime_opensecchreq.hex test-messages/datetime_hex/datetime_getendpointsreq.hex
#test-messages/datetime_hex/datetime_ack.hex test-messages/datetime_hex/datetime_error.hex

build/opc_ua.hlto: spicy/*
	spicyz -d -o build/opc_ua.hlto spicy/opc_ua*.spicy spicy/opc_ua.evt

build/opc_ua.out: spicy/*
	spicyc -R -c -o opc_ua.tmp spicy/opc_ua*.spicy
	spicy-build -o build/opc_ua.out -v opc_ua.cxx
	chmod +x build/opc_ua.out
	rm opc_ua.tmp

test: build/opc_ua.hlto clean-zeek
	mkdir -p zeek-test && cd zeek-test &&\
	zeek -C -r ../test-messages/single-time-request.pcapng ../build/opc_ua.hlto ../__load__.zeek "Spicy::enable_print=T; redef LogAscii::use_json=T;"

test-zeek-jit: clean-zeek
	mkdir -p zeek-test && cd zeek-test &&\
	zeek -C -r ../test-messages/single-time-request.pcapng ../spicy/opc_ua*.spicy ../__load__.zeek "Spicy::enable_print=T"

test-out: opc_ua.out
	for hex in ${INPUT} ; do \
		echo -n "xxd for [$$hex]:\n" ; \
		xxd -g 1 -b "$$hex" ; \
		echo "opc_ua.out for [$$hex]:" ; \
		cat "$$hex" | ./opc_ua.out ; \
		echo "" ; \
	done;

# TODO use spicyc
test-jit:
	for hex in ${INPUT} ; do \
		echo -n "xxd for [$$hex]:\n" ; \
		xxd -g 1 -b "$$hex" ; \
		echo -n "spicy-driver for [$$hex]:\n" ; \
		cat "$$hex" | spicy-driver spicy/opc_ua*.spicy ; \
		echo "" ; \
	done;

shell:
	@mkdir -p zeek-test && cd zeek-test &&\
	echo -n "Default test command: " && grep -A2 '^test:' ../Makefile | grep -Po '\bzeek .*$$' &&\
	PROMPT_COMMAND='echo "$$PS1" | grep "^[(]zeek[)] " >/dev/null || PS1="(zeek) $$PS1"' bash

clean: clean
	rm opc_ua.hlto

clean-zeek:
	rm -rf zeek-test
