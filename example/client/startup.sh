#!/bin/sh

echo "Connection to opc.tcp://${SERVER:-localhost}:${PORT:-4840} every ${SLEEP:-120} seconds...."

while true; do
	/opt/open62541/build/bin/examples/client_connect "opc.tcp://${SERVER:-localhost}:${PORT:-4840}"
	sleep "${SLEEP:-120}"
done
