# OPC UA Dissector

## Build parser
Creates a file "opc_ua.hlto" that contains the compiled parser.
```
make
```

## Run tests
Builds the parser and runs it with Zeek, using the sample file single-time-request.pcapng.
```
make test
```

## Run custom tests
Drops you into a shell in the correct directory where you can run custom commands without polluting any directory with log files.
```
make shell
```

## Install in Zeek
```
ZEEKROOT=/opt/zeek
cp opc_ua.hlto "$ZEEKROOT/lib/zeek/plugins/Zeek_Spicy/modules"
cp opc_ua.zeek "$ZEEKROOT/share/zeek/site/"
echo "@load opc_ua" >> "$ZEEKROOT/share/zeek/site/local.zeek"
```
