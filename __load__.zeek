module OPC_UA;

export {
    # Create an ID for our new stream. By convention, this is
    # called "LOG".
    redef enum Log::ID += { LOG };

    # Define the record type that will contain the data to log.
    type Info: record {
        ts: time        &log;
        id: conn_id     &log;
        is_orig: bool   &log;
        event_type: string &log;
        req_id: count &log &optional;
        msg_type: string &log &optional;
        msg_size: count &log &optional;
        reserved: string &log &optional;

        sec_chann_id: count &log &optional;
        seq_number: count &log &optional;

        protocol_version: count &log &optional;
        endpoint_url: string &log &optional;
        server_url: string &log &optional;
        error: count &log &optional;
        reason: string &log &optional;
        length: count &log &optional;
    };

    global log_opc_ua: event(rec: Info);
}

# Optionally, we can add a new field to the connection record so that
# the data we are logging (our "Info" record) will be easily
# accessible in a variety of event handlers.
redef record connection += {
    # By convention, the name of this new field is the lowercase name
    # of the module.
    opc_ua: Info &optional;
};

# This event is handled at a priority higher than zero so that if
# users modify this stream in another script, they can do so at the
# default priority of zero.
event zeek_init() &priority=5 {
    # Create the stream. This adds a default filter automatically.
    Log::create_stream(OPC_UA::LOG, [$columns=Info, $path="opc_ua", $ev=log_opc_ua]);
}

# CONNECTION PROTOCOL

event opc_ua_tcp::header_con(c: connection, is_orig: bool, msg_type: string, msg_size: count, reserved: string) {
    local rec: OPC_UA::Info = [$ts=network_time(), $id=c$id, $is_orig=is_orig, $event_type="header_con", $msg_type=msg_type, $msg_size=msg_size, $reserved=reserved];
	# Store a copy of the data in the connection record so other event handlers can access it.
	c$opc_ua = rec;
	Log::write(LOG, rec);
}

event opc_ua_tcp::body_con_hel(c: connection, is_orig: bool, protocol_version: count, receiver_buffer_size: count, send_buffer_size: count, max_message_size: count, max_chunk_count: count, len_endpoint: int, endpoint_url: string) {
	local rec: OPC_UA::Info = [$ts=network_time(), $id=c$id, $is_orig=is_orig, $event_type="body_con_hel", $protocol_version=protocol_version, $endpoint_url=endpoint_url];
	Log::write(LOG, rec);
}

event opc_ua_tcp::body_con_ack(	c: connection, is_orig: bool, protocol_version: count, receiver_buffer_size: count, send_buffer_size: count, max_message_size: count, max_chunk_count: count) {
	local rec: OPC_UA::Info = [$ts=network_time(), $id=c$id, $is_orig=is_orig, $event_type="body_con_ack", $protocol_version=protocol_version];
	Log::write(LOG, rec);
}

event opc_ua_tcp::body_con_err(c: connection, is_orig: bool, error: count, len_reason: int, reason: string) {
	local rec: OPC_UA::Info = [$ts=network_time(), $id=c$id, $is_orig=is_orig, $event_type="body_con_err", $error=error, $reason=reason];
	Log::write(LOG, rec);
}

event opc_ua_tcp::body_con_rhe(c: connection, is_orig: bool, len_uri: int, server_uri: string, len_url: int, endpoint_url: string) {
	local rec: OPC_UA::Info = [$ts=network_time(), $id=c$id, $is_orig=is_orig, $event_type="body_con_rhe", $server_url=server_uri, $endpoint_url=endpoint_url];
	Log::write(LOG, rec);
}

# SECURE CONVERSATION

event opc_ua_tcp::header_sec_msg(c: connection, is_orig: bool, sec_chann_id: count, msg_type: string, msg_size: count, is_final: string) {
    local rec: OPC_UA::Info = [$ts=network_time(), $id=c$id, $is_orig=is_orig, $event_type="header_sec_msg", $msg_type=msg_type, $msg_size=msg_size, $sec_chann_id=sec_chann_id, $reserved=is_final];
	Log::write(LOG, rec);
}

event opc_ua_tcp::header_sec_seq(c: connection, is_orig: bool, req_id: count, seq_number: count) {
    local rec: OPC_UA::Info = [$ts=network_time(), $id=c$id, $is_orig=is_orig, $event_type="header_sec_seq", $req_id=req_id, $seq_number=seq_number];
	Log::write(LOG, rec);
}


event opc_ua_tcp::body_sec_msg(c: connection, is_orig: bool, body: string) {
	local rec: OPC_UA::Info = [$ts=network_time(), $id=c$id, $is_orig=is_orig, $event_type="body_sec_msg", $length=|body|];
	Log::write(LOG, rec);
}

event opc_ua_tcp::body_sec_opn(c: connection, is_orig: bool, body: string) {
	local rec: OPC_UA::Info = [$ts=network_time(), $id=c$id, $is_orig=is_orig, $event_type="body_sec_opn", $length=|body|];
	Log::write(LOG, rec);

}
event opc_ua_tcp::body_sec_clo(c: connection, is_orig: bool, body: string) {
	local rec: OPC_UA::Info = [$ts=network_time(), $id=c$id, $is_orig=is_orig, $event_type="body_sec_clo", $length=|body|];
	Log::write(LOG, rec);
}
